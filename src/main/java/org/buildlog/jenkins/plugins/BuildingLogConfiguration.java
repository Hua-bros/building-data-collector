package org.buildlog.jenkins.plugins;

import hudson.Extension;
import hudson.util.FormValidation;
import jenkins.model.GlobalConfiguration;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.buildlog.jenkins.plugins.uitl.HttpUtil;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.verb.POST;

import java.io.IOException;


@Extension
public class BuildingLogConfiguration extends GlobalConfiguration {
    private String hostName;
    private int port;
    private String transferUrl;
    private String healthCheckUrl;


    @DataBoundConstructor
    public BuildingLogConfiguration() {
        load(); // load the persisted global configurations

    }

    @Override
    public String getDisplayName() {
        return "BuildingLog plugin";
    }


    @Override
    public boolean configure(StaplerRequest req, JSONObject json) throws FormException {
        hostName = json.getString("hostName");
        port = json.getInt("port");
        transferUrl = json.getString("transferUrl");
        healthCheckUrl = json.getString("healthCheckUrl");

        // Persist global configuration information
        save();

        return super.configure(req,json);
    }

    @POST
    public FormValidation doTestConnection(@QueryParameter("hostName") String hostName,
                                           @QueryParameter("port") int port,
                                           @QueryParameter("healthCheckUrl") String healthCheckUrl)
            throws IOException {
        FormValidation validate = validate(hostName, port, healthCheckUrl, "dummy");
        if (FormValidation.ok().kind == validate.kind) {
            if (HttpUtil.testConnection(hostName, port, healthCheckUrl)) {
                return FormValidation.okWithMarkup("connection established");
            } else {
                return FormValidation.error("can not establish  connection to:   " + hostName + ":" + port);
            }
        } else {
            return validate;
        }
    }

    private FormValidation validate(String hostName, int port, String healthChekUrl, String transferUrl) {
        String errorMessage = "";
        if (port <= 0) {
            errorMessage = "parameter port must be a positive number.";
        } else if (StringUtils.isBlank(hostName)) {
            errorMessage = "parameter hostname can not be blank.";
        } else if (StringUtils.isBlank(healthChekUrl)) {
            errorMessage = "parameter healthCheckUrl can not be blank.";
        } else if (StringUtils.isBlank(transferUrl)) {
            errorMessage = "parameter transferUrl can not be blank.";
        }

        if (errorMessage.length() > 0) {
            return FormValidation.errorWithMarkup(errorMessage);
        } else {
            return FormValidation.ok();
        }
    }

    public String getHostName() {
        return hostName;
    }

    @DataBoundSetter
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    @DataBoundSetter
    public void setPort(int port) {
        this.port = port;
    }

    public String getTransferUrl() {
        return transferUrl;
    }

    @DataBoundSetter
    public void setTransferUrl(String transferUrl) {
        this.transferUrl = transferUrl;
    }

    public String getHealthCheckUrl() {
        return healthCheckUrl;
    }

    @DataBoundSetter
    public void setHealthCheckUrl(String healthCheckUrl) {
        this.healthCheckUrl = healthCheckUrl;
    }
}
