package org.buildlog.jenkins.plugins.model;

import java.io.Serializable;
import java.util.Map;

public class BuildingData implements Serializable {
    private static final long serialVersionUID = 34534234L;

    private String currentUser;
    private String jobName;
    private String buildNumber;
    private String opTime;
    private String consoleLog;
    private Map<String, String> buildVariables;

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getConsoleLog() {
        return consoleLog;
    }

    public Map<String, String> getBuildVariables() {
        return buildVariables;
    }

    public String getOpTime() {
        return opTime;
    }

    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }

    public void setBuildVariables(Map<String, String> buildVariables) {
        this.buildVariables = buildVariables;
    }

    public void setConsoleLog(String consoleLog) {
        this.consoleLog = consoleLog;
    }
}
