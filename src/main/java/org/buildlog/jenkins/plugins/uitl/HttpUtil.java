package org.buildlog.jenkins.plugins.uitl;

import hudson.ProxyConfiguration;
import jenkins.model.Jenkins;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;

public final class HttpUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtil.class);

    private HttpUtil() {
        throw new AssertionError("create instance of this class is prohibited!");
    }


    public static void post(URL url, String json) throws IOException {
        HttpURLConnection httpURLConnection = getHttpURLConnection(url);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8");
        httpURLConnection.setRequestProperty("Accept", "application/json");
        httpURLConnection.setDoOutput(true);

        try (OutputStream os = httpURLConnection.getOutputStream()) {
            byte[] bytes = json.getBytes("utf-8");
            os.write(bytes, 0, bytes.length);

            LOGGER.info("send post request to   " + url.toString());
            LOGGER.info("posting data is:" + json);
        }
        try (BufferedReader reader =
                     new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"))) {
            StringBuilder builder = new StringBuilder();
            String responseLine = null;
            while ((responseLine = reader.readLine()) != null) {
                builder.append(responseLine.trim());
            }
            LOGGER.info(builder.toString());
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
    }

    public static HttpURLConnection getHttpURLConnection(final URL url) throws IOException {
        HttpURLConnection conn = null;
        ProxyConfiguration proxyConfig = null;

        Jenkins jenkins = Jenkins.getInstanceOrNull();
        if (jenkins != null) {
            proxyConfig = jenkins.proxy;
        }

        if (proxyConfig != null) {
            Proxy proxy = proxyConfig.createProxy(url.getHost());
            if (proxy != null && proxy.type() == Proxy.Type.HTTP) {
                conn = (HttpURLConnection) url.openConnection(proxy);
            }
        }

        if (conn == null) {
            conn = (HttpURLConnection) url.openConnection();
        }

        int timeout = 1 * 60 * 1000;

        conn.setConnectTimeout(timeout);
        conn.setReadTimeout(timeout);

        return conn;
    }

    public static URL createURL(String host, int port, String uri) throws MalformedURLException {
        if (host.endsWith("/")) {
            host = host.substring(0, host.lastIndexOf("/"));
        }

        uri = uri.startsWith("/") ? uri : "/" + uri;

        URL url;
        if (host.startsWith("http")) {
            url = new URL(host + ":" + port + uri);
        } else {
            url = new URL("http", host, port, uri);
        }
        return url;
    }

    public static boolean testConnection(String host, int port, String uri) throws IOException {
        HttpURLConnection conn = null;
        try {
            URL url = HttpUtil.createURL(host, port, uri);
            System.out.println(url.toString());
            conn = HttpUtil.getHttpURLConnection(url);
            conn.connect();

            return HttpURLConnection.HTTP_OK == conn.getResponseCode();
        } catch (Exception e) {
            return false;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

}
