package org.buildlog.jenkins.plugins.uitl;

import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public final class DateTimeUtil {
    private DateTimeUtil() {
        throw new AssertionError("create instance of this class is prohibited!");
    }


    public  static final String FULL_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";


    /**
     * formatting a calendar instance to string over the given format.
     *
     * @param cal
     * @param format
     * @return
     */
    public static String formatCalendar(Calendar cal, String format) {
        if (cal == null) {
            return StringUtils.EMPTY;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        String result = dateFormat.format(cal.getTime());
        return result;
    }
}
