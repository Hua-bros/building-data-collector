package org.buildlog.jenkins.plugins.uitl;

import hudson.ExtensionList;
import org.buildlog.jenkins.plugins.BuildingLogConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class BuildingLogUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(BuildingLogUtil.class);

    private BuildingLogUtil() {
    }


    /**
     * @return the global configuration of this plugin.
     */
    public static BuildingLogConfiguration getBuildingLogGlobalConfiguration() {
        try {
            return ExtensionList.lookupSingleton(BuildingLogConfiguration.class);
        } catch (IllegalStateException | NullPointerException e) {
            LOGGER.error("lookup BuildingLogConfiguration instance failed," + e.getMessage());
            return null;
        }
    }

}
