package org.buildlog.jenkins.plugins;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public final class DataSenderExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSenderExecutor.class);
    private static final int SHUTDOWN_TIME = 1;


    private ExecutorService executorService;
    private static DataSenderExecutor INSTANCE = null;

    private DataSenderExecutor() {
        executorService = Executors.newFixedThreadPool(3);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            executorService.shutdown();
            try {
                if (!executorService.awaitTermination(SHUTDOWN_TIME, TimeUnit.MINUTES)) {
                    LOGGER.info("Executor did not terminate in the specified time.");
                    List<Runnable> droppedTasks = executorService.shutdownNow();
                    LOGGER.warn("Executor was abruptly shut down. " + droppedTasks.size() + " tasks will not be executed.");
                }
            } catch (InterruptedException e) {
                LOGGER.error("shutdownHook error:", e);
            }
        }));
    }

    public synchronized static DataSenderExecutor getInstance() {
        if (INSTANCE == null) {
            synchronized (DataSenderExecutor.class) {
                INSTANCE = new DataSenderExecutor();
            }
        }
        return INSTANCE;
    }


    public void sendData(DataSender dataSender) {
        this.executorService.execute(dataSender);
    }

}
