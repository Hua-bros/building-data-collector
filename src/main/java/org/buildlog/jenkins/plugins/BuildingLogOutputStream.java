package org.buildlog.jenkins.plugins;

import hudson.console.LineTransformationOutputStream;
import org.buildlog.jenkins.plugins.model.BuildingData;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;


public class BuildingLogOutputStream extends LineTransformationOutputStream {

    private OutputStream delegate;

    public BuildingLogOutputStream(OutputStream delegate) {
        this.delegate = delegate;
    }

    /**
     * Called for each end of the line
     *
     * @param b
     * @param len
     * @throws IOException
     */
    @Override
    protected void eol(byte[] b, int len) throws IOException {
        // do not lost the data
        delegate.write(b);
        delegate.flush();

        String line = new String(b, 0, len, Charset.defaultCharset());

        BuildingData data = ContextManager.getData();
        data.setConsoleLog(data.getConsoleLog() + line);
        ContextManager.setData(data);
    }
}
