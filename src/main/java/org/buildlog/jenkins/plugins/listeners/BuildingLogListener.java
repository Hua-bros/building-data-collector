package org.buildlog.jenkins.plugins.listeners;

import hudson.Extension;
import hudson.Launcher;
import hudson.model.*;
import hudson.model.listeners.RunListener;
import org.buildlog.jenkins.plugins.ContextManager;
import org.buildlog.jenkins.plugins.DataSender;
import org.buildlog.jenkins.plugins.DataSenderExecutor;
import org.buildlog.jenkins.plugins.model.BuildingData;
import org.buildlog.jenkins.plugins.uitl.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;


/**
 * Receives notifications about builds.
 */
@Extension
public class BuildingLogListener extends RunListener<Run> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BuildingLogListener.class);

    public BuildingLogListener(Class<Run> targetType) {
        super(targetType);
    }

    public BuildingLogListener() {
        super();
    }


    // Run#onStartBuilding fires RunListener#fireInitialize
    @Override
    public void onInitialize(Run run) {
        super.onInitialize(run);

        BuildingData buildingData = new BuildingData();
        buildingData.setBuildNumber(run.number + "");
        buildingData.setConsoleLog("");
        ContextManager.setData(buildingData);
    }

    @Override
    public void onStarted(Run run, TaskListener listener) {
        super.onStarted(run, listener);

    }

    @Override
    public void onCompleted(Run run, TaskListener listener) {
        super.onCompleted(run, listener);
    }


    // Run#onEndBuilding fires RunListener#onFinalized
    @Override
    public void onFinalized(Run run) {
        super.onFinalized(run);

        try {
            final BuildingData data = ContextManager.getData();
            DataSenderExecutor.getInstance().sendData(new DataSender(data));
        } finally {
            ContextManager.removeData();
        }
    }


    @Override
    public Environment setUpEnvironment(AbstractBuild build, Launcher launcher, hudson.model.BuildListener listener)
            throws IOException, InterruptedException, Run.RunnerAbortedException {

        Set<User> users = build.calculateCulprits();
        Map<String, String> buildVariables = build.getBuildVariables();
        AbstractProject<?, ?> project = build.getProject();
        Optional<User> u = users.stream().filter(Objects::isNull).findAny();

        BuildingData data = ContextManager.getData();
        data.setOpTime(DateTimeUtil.formatCalendar(build.getTimestamp(), DateTimeUtil.FULL_DATETIME_FORMAT));
        data.setCurrentUser(u.isPresent() ? u.get().getFullName() : "anonymous");
        data.setJobName(project.getName());
        data.setBuildVariables(buildVariables);

        ContextManager.setData(data);

        return super.setUpEnvironment(build, launcher, listener);
    }

    @Override
    public void onDeleted(Run run) {
        super.onDeleted(run);
    }

}
