package org.buildlog.jenkins.plugins;

import org.buildlog.jenkins.plugins.model.BuildingData;

public final class ContextManager{

    private ContextManager(){}

    private static ThreadLocal<BuildingData> data = new ThreadLocal<>();

    public static void setData(BuildingData buildingData){
        data.set(buildingData);
    }

    public static BuildingData getData(){
        return data.get();
    }

    public static void removeData(){
        data.remove();
    }

}
