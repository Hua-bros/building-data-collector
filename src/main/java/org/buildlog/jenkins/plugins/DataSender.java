package org.buildlog.jenkins.plugins;

import com.google.gson.Gson;
import org.buildlog.jenkins.plugins.model.BuildingData;
import org.buildlog.jenkins.plugins.uitl.BuildingLogUtil;
import org.buildlog.jenkins.plugins.uitl.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class DataSender implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSender.class);

    private String host = BuildingLogUtil.getBuildingLogGlobalConfiguration().getHostName();
    private int port = BuildingLogUtil.getBuildingLogGlobalConfiguration().getPort();
    private String url = BuildingLogUtil.getBuildingLogGlobalConfiguration().getTransferUrl();

    private BuildingData buildData;

    public DataSender(BuildingData buildData) {
        this.buildData = buildData;
    }

    @Override
    public void run() {
        try {
            Gson gson = new Gson();
            final String json = gson.toJson(this.getBuildData());
            HttpUtil.post(HttpUtil.createURL(this.host, this.port, this.url), json);
        } catch (IOException e) {
            LOGGER.error("error occurred when sending data:" + e.getCause());
        }
    }

    public BuildingData getBuildData() {
        return buildData;
    }
}
