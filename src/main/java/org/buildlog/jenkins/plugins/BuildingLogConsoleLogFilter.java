package org.buildlog.jenkins.plugins;


import hudson.Extension;
import hudson.console.ConsoleLogFilter;
import hudson.model.Run;

import java.io.IOException;
import java.io.OutputStream;

@Extension
public class BuildingLogConsoleLogFilter extends ConsoleLogFilter {


    // this method runs within the same thread with RunListener
    @Override
    public OutputStream decorateLogger(Run build, OutputStream logger) throws IOException, InterruptedException {
        return new BuildingLogOutputStream(logger);
    }
}
