package org.buildlog.jenkins.plugins.listeners;


import hudson.ExtensionList;
import hudson.model.FreeStyleBuild;
import hudson.model.FreeStyleProject;
import hudson.model.listeners.RunListener;
import org.buildlog.jenkins.plugins.ContextManager;
import org.buildlog.jenkins.plugins.model.BuildingData;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class BuildingLogListenerTest {


    @Rule
    public JenkinsRule j = new JenkinsRule();


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testOnInitialize() throws IOException {
        test();
    }

    private void test() throws IOException {
        /*CLICommandInvoker.Result result = new CLICommandInvoker(j, "create-job").
                withStdin(new ByteArrayInputStream(("<project><actions/><builders/><publishers/><buildWrappers/></project>").getBytes())).
                invokeWithArgs("testJob");*/

        final FreeStyleProject project = j.createFreeStyleProject("testJob");
        FreeStyleBuild build = new FreeStyleBuild(project);
        build.number = 990;

        final BuildingLogListener listener = ExtensionList.lookupSingleton(BuildingLogListener.class);
        RunListener.all().add(0,listener);
        RunListener.fireInitialize(build);

        final BuildingData data = ContextManager.getData();
        assertNotNull(data);
        assertTrue(data.getBuildNumber().equals(build.number+""));
    }


    @Test
    public void testOnFinalized() throws IOException {
        test();
    }

    @Test
    public void testSetUpEnvironment() throws IOException {
        test();
    }
}