package org.buildlog.jenkins.plugins.uitl;

import hudson.ExtensionList;
import hudson.model.FreeStyleBuild;
import hudson.model.FreeStyleProject;
import hudson.model.listeners.RunListener;
import org.buildlog.jenkins.plugins.BuildingLogConfiguration;
import org.buildlog.jenkins.plugins.listeners.BuildingLogListener;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.jvnet.hudson.test.JenkinsRule;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

public class BuildingLogUtilTest{


    @Rule
    public JenkinsRule j = new JenkinsRule();


    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Test
    public void testGetBuildingLogGlobalConfiguration() throws IOException {

        final FreeStyleProject project = j.createFreeStyleProject("testJob");
        FreeStyleBuild build = new FreeStyleBuild(project);

        final BuildingLogListener listener = ExtensionList.lookupSingleton(BuildingLogListener.class);
        RunListener.all().add(0,listener);
        RunListener.fireInitialize(build);

        final BuildingLogConfiguration configuration = ExtensionList.lookupSingleton(BuildingLogConfiguration.class);
        assertNotNull(configuration);

        j.jenkins.cleanUp();
        exception.expect(IllegalStateException.class);
        ExtensionList.lookupSingleton(BuildingLogConfiguration.class);
    }
}