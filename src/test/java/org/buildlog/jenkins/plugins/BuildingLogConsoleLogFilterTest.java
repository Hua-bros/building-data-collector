package org.buildlog.jenkins.plugins;

import hudson.ExtensionList;
import hudson.console.LineTransformationOutputStream;
import hudson.model.FreeStyleBuild;
import hudson.model.FreeStyleProject;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.model.listeners.RunListener;
import org.buildlog.jenkins.plugins.listeners.BuildingLogListener;
import org.junit.Rule;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;
import org.jvnet.hudson.test.TestExtension;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;

import static org.junit.Assert.assertNotNull;

public class BuildingLogConsoleLogFilterTest {


    @Rule
    public JenkinsRule j = new JenkinsRule();


    @Test
    public void decorateSlaveLog() throws Exception {
        final FreeStyleProject project = j.createFreeStyleProject("testJob");
        FreeStyleBuild build = new FreeStyleBuild(project);

        final BuildingLogListener listener = ExtensionList.lookupSingleton(BuildingLogListener.class);
        RunListener.all().add(0, listener);

        final TaskListener taskListener = (TaskListener) () -> System.out;
        RunListener.fireCompleted(build, taskListener);

        final PrintStream printStream = taskListener.getLogger();
        byte[] buffer = new byte[256];
        printStream.write(buffer);

        String logs = new String(buffer, Charset.defaultCharset());
        assertNotNull(logs.contains("localhost"));
    }

    @TestExtension
    public static class Impl extends BuildingLogConsoleLogFilter {
        @Override
        public OutputStream decorateLogger(Run build, OutputStream logger) throws IOException, InterruptedException {
            return new LineTransformationOutputStream.Delegating(logger) {
                @Override
                protected void eol(byte[] b, int len) throws IOException {
                    out.write(("localhost").getBytes());
                    out.write(b, 0, len);
                }
            };
        }
    }

}